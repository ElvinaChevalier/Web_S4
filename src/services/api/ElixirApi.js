const getElixirData = async function() {
  const response = await fetch("https://wizard-world-api.herokuapp.com/Elixirs")
  if (response.status == 200) {
    let data=await response.json()
    return data 
  }else {
    new Error(response.statusText)
  }
} 
export default getElixirData